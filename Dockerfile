FROM python:3.10-alpine

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV TF_PLUGIN_CACHE_DIR /terraform_cache/
ENV TF_CLI_CONFIG_FILE /terraform_cache/.terraformrc

CMD [ "/app/start.sh" ]

# Dependencies:
# - terraform: remote VM management
# - gcc, make…: compile Python dependencies
# - curl, OpenSSH…: communicate with the VMs and other machines
RUN apk add --no-cache \
	terraform bash docker-cli \
    gcc make build-base libffi-dev musl-dev \
    curl openssh openssl openssl-dev \
    py3-paramiko python3-dev \
    wireguard-tools

# Copy only the requirements file, because it doesn't change often
# The other files will be copied later, because they change often and are not required to install dependencies
# (which don't change often either)
COPY web/requirements.txt /app/requirements.txt
RUN cd /app \
 && python3 -m venv venv \
 && . venv/bin/activate \
 && pip install --upgrade pip \
 && pip install -r requirements.txt

# Copy our pre-downloaded terraform module cache (needed for offline operation)
COPY .terraform/providers/registry.terraform.io /terraform_cache/registry.terraform.io
COPY .terraformrc /terraform_cache/.terraformrc

# Copy all Python, templates, static files, etc
COPY web /app

# Include the challenges in the container
COPY challenges /app/challenges
# Include the help in the container
COPY help /app/help
