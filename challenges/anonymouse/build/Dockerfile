FROM ubuntu:latest

# Update and install openssh-server
RUN apt-get update && \
    apt-get install -y gcc sudo nano openssh-server && \
    mkdir /var/run/sshd

# Create the user user with no password required
RUN useradd -m user --shell /bin/bash -p $(echo user | openssl passwd -1 -stdin) && \
    chmod 500 /home/user

# Giving access to the shadow file
RUN usermod -a -G shadow user

# Create level1 user with password "Passw@rd1234"
RUN useradd -m level1 --shell /bin/bash -p $(echo 'Passw@rd1234' | openssl passwd -1 -stdin) && \
    chmod 500 /home/level1

# Create accoutant user with password "level2"
RUN useradd -m level2 --shell /bin/bash -p $(echo 'FLAGY9$dlj?D*j1=b?e*IqKr' | openssl passwd -1 -stdin) && \
    chmod 500 /home/level2

# Create level3 user with password "level3"
RUN useradd -m level3 --shell /bin/bash -p $(echo '7P75pTIV0mif6uoF0OPKsMFIThisIsFlagufKT' | openssl passwd -1 -stdin) && \
    chmod 500 /home/level3

# Create level4 user with password "level4"
RUN useradd -m level4 --shell /bin/bash -p $(echo 'FLAGauJ8O!pbThNM7-R_pWe7' | openssl passwd -1 -stdin) && \
    chmod 500 /home/level4

# Configure user with its challenge
COPY resources/dictionnary.txt /home/user/dictionnary.txt

# Configure level2 user with its challenge
COPY resources/ciphered_flag.txt /home/level2/ciphered_flag.txt
RUN chown level2 /home/level2/ciphered_flag.txt

# Configure level3 user with its challenge
COPY resources/script.c /home/level3/script.c
RUN gcc -o /home/level3/script /home/level3/script.c && \
    chmod 511 /home/level3/script && \
    chmod 504 /home/level3/script.c

RUN echo 'FLAGauJ8O!pbThNM7-R_pWe7' > /home/level3/.passwd

RUN chmod 500 /home/level3/.passwd && \
    chown root:root /home/level3/script && \
    chmod u+s /home/level3/script

RUN echo "level3 ALL = (root) NOPASSWD: /home/level3/script">> /etc/sudoers && \
    echo "level3 ALL = (root) !/bin/ls /home/level3/.passwd">> /etc/sudoers && \
    echo "level3 ALL = (root) !/bin/cat /home/level3/.passwd">> /etc/sudoers

# Configure level4 user with its challenge
COPY resources/webFiles/index.html /home/level4/
COPY resources/webFiles/style.css /home/level4/
COPY resources/webFiles/mouse.png /home/level4/


# Expose the ssh port
EXPOSE 22


# Start the ssh server
CMD ["/usr/sbin/sshd", "-D"]
