terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

resource "docker_container" "docker_vassago" {
  image = "${var.docker_registry}/vassago:${var.build_version}"
  name  = "docker_${var.instance_id}_vassago"
  ports {
    internal = 3000
  }
  env = [ "USER_TOKEN=${random_string.flag_user_token.result}", "ADMIN_TOKEN=${random_string.flag_admin_token.result}", "ADMIN_PASSWORD=${random_shuffle.flag_admin_password.result[0]}" ]
}

#
# Génération des flag
#
resource "random_string" "flag_user_token" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_user_token.result} > ${path.module}/flags/flag_user_token.txt"
  }
}

resource "random_string" "flag_admin_token" {
  keepers = {instance_id = var.instance_id}
  length = 32
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag_admin_token.result} > ${path.module}/flags/flag_admin_token.txt"
  }
}

resource "random_shuffle" "flag_admin_password" {
  keepers = { instance_id = var.instance_id }
  input = ["password123", "123456", "password", "abcdef", "password1", "666666", "987654321"]
  result_count = 1
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_shuffle.flag_admin_password.result[0]} > ${path.module}/flags/flag_admin_password.txt"
  }
}

variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "build_version" {
  type = string
}

variable "docker_registry" {
  type = string
}

output "port" {
  value = docker_container.docker_vassago.ports[0].external
}
