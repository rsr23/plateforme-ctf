class AddPasswordHintToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :hint, :string
  end
end
