<!DOCTYPE html>
<html>
<body>

<?php
// Check if the form has been submitted
if(isset($_POST['submit'])) {
    // Get the uploaded file
    $file = $_FILES['coquillage'];
    $file_name = $file['name'];
    $file_tmp = $file['tmp_name'];

    // Get file extension
    $file_ext = explode('.', $file_name);
    $file_ext = strtolower(end($file_ext));
    
    // Allowed file extensions
    $allowed = array('png');

    if($_FILES['coquillage']['type'] == 'image/png') {
        // Generate a new file name
        $file_name_new = $file_name;
        // Set the file destination
        $file_destination = 'uploaded/' . $file_name_new;

        // Move the uploaded file to the destination
        move_uploaded_file($file_tmp, $file_destination);

        // File was uploaded successfully
        echo 'Ça roule nickel mon brave !';
    } else {
        // File type is not allowed
        echo 'Y a eu un problème...';
    }
}
?>

<a href="azertyuiopqsdfgh.php">Revenir à la page d'accueil.</a>

</body>
</html>