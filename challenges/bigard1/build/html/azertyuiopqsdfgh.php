<!DOCTYPE html>
<html>
<body>

<h1>Galerie photos de mes coquillages :</h1>

<p>
Voici ma merveilleuse galerie photo de mes coquillages récupérés au Cap Ferret !
</p>

<form action="upload.php" method="post" enctype="multipart/form-data">
    Sélectionner une image à ajouter à la galerie de coquillages (PNG uniquement !) : <br>
    <input type="file" value="Chercher l'image" name="coquillage" id="coquillage"> <br>
    <input type="submit" value="Envoyer l'image" name="submit">
</form>

<br>

<?php

// The directory where the files are stored
$dir = 'uploaded/';

// Open the directory
$files = scandir($dir);

// Loop through the files
foreach ($files as $file) {

    // Ignore the current and parent directories
    if ($file === '.' || $file === '..') {
        continue;
    }
    
    // Check if the file is a PNG image
    echo '<a href="' . $dir . $file . '"><img src="' . $dir . $file . '" alt="' . $file . '" width="200" height="200"></a>';
}

?>




</body>
</html>