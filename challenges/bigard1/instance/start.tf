terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

resource "docker_container" "docker_bigard1" {
  image = "${var.docker_registry}/bigard1:${var.build_version}"
  name  = "docker_${var.instance_id}_bigard1"
  upload {
    file    = "/var/flag.txt"
    content = random_string.flag.result
  }

  ports {
    internal = 80
  }
}

#
# Génération des flag
#
resource "random_string" "flag" {
  keepers = {instance_id = var.instance_id}
  length = 16
  special = false
  min_upper = 4
  min_lower = 4
  min_numeric = 4
  provisioner "local-exec" {
    command = "mkdir -p ${path.module}/flags; echo ${random_string.flag.result} > ${path.module}/flags/flag.txt"
  }
}


variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "build_version" {
  type = string
}

variable "docker_registry" {
  type = string
}

output "port" {
  value = docker_container.docker_bigard1.ports[0].external
}
