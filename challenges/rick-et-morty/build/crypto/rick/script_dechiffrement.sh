#!/bin/bash

# Vérifie si la présence d'un nom de dossier et de la clé de déchiffrement
if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Erreur, format attendu : ./script_dechiffrement.sh nom_dossier cle_dechiffrement"
  exit 1
fi

#"${1%/}" enlève le / à la fin du nom si présent
nom_dossier="${1%/}"
cle_dechiffrement="$2"
nom_dossier_dechiffre=$nom_dossier"_dechiffre"
hash_entre=$(echo -n "$2" | md5sum -b | awk '{print $1}')

#Etape facultative de vérifier si le hash md5 correspond a la clé entrée pour éviter de faire un mauvais déchiffrement
if [ -f $nom_dossier/hash.txt ]; then
    echo "Hash trouvé"
    hash_file="$nom_dossier/hash.txt"
    read hash_read < $hash_file
    if [ "$hash_read" != "$hash_entre" ]; then
      echo "Les hash ne correspondent pas, annulation du dechiffrement"
      exit 1
    else 
      echo "Les hash correspondent"
    fi
fi

# Vérifie si le dossier source existe
if [ ! -d "$nom_dossier" ]; then
  echo "Le dossier $nom_dossier n'existe pas."
  exit 1
fi

# Vérifie si le dossier cible n'existe pas déjà
if [ -d $nom_dossier_dechiffre ]; then
    echo "Le dossier $nom_dossier_dechiffre existe. Annulation"
    exit 1
fi

mkdir $nom_dossier_dechiffre

# Exécute la commande pour tous les fichiers
for file in $(find "$nom_dossier" -type f); do
    #echo "./shift -u -r $file -s $cle_dechiffrement > $PWD/$nom_dossier_dechiffre/$file"
    reverse_path=$(echo $file | rev)
    # coupe la chaine jusqu'au dernier /
    nom_fichier=$(echo $reverse_path | cut -d '/' -f 1 | rev)
    chemin_fichier=$(echo $file | sed "s/$nom_fichier//")
    #echo "mkdir -p $PWD/$nom_dossier_dechiffre/$chemin_fichier"
    mkdir -p $PWD/$nom_dossier_dechiffre/$chemin_fichier
    #echo "./shift -u -r $file -s $cle_dechiffrement > $PWD/$nom_dossier_dechiffre/$file"
    ./shift -u -r $file -s $cle_dechiffrement > $PWD/$nom_dossier_dechiffre/$file
done