#!/bin/bash

# Vérifie si la présence d'un nom de dossier et de la clé de chiffrement
if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Erreur, format attendu : ./script_chiffrement.sh nom_dossier cle_chiffrement"
  exit 1
fi

#"${1%/}" enlève le / à la fin du nom si présent
nom_dossier="${1%/}"
cle_chiffrement="$2"
nom_dossier_chiffre=$nom_dossier"_chiffre"

# Vérifie si le dossier source existe
if [ ! -d "$nom_dossier" ]; then
  echo "Le dossier $nom_dossier n'existe pas."
  exit 1
fi

# Vérifie si le dossier cible n'existe pas déjà
if [ -d $nom_dossier_chiffre ]; then
    echo "Le dossier $nom_dossier_chiffre existe. Annulation"
    exit 1
fi

mkdir $nom_dossier_chiffre

# Exécute la commande pour tous les fichiers
for file in $(find "$nom_dossier" -type f); do
  #echo "./shift -r $file -s $cle_chiffrement > $PWD/$nom_dossier_chiffre/$file"
  reverse_path=$(echo $file | rev)
  # coupe la chaine jusqu'au dernier /
  nom_fichier=$(echo $reverse_path | cut -d '/' -f 1 | rev)
  chemin_fichier=$(echo $file | sed "s/$nom_fichier//")
  #echo "$nom_fichier"
  #echo "$chemin_fichier"
  #echo "mkdir -p $PWD/$nom_dossier_chiffre/$chemin_fichier"
  mkdir -p $PWD/$nom_dossier_chiffre/$chemin_fichier
  #echo "touch $PWD/$nom_dossier_chiffre/$file"
  #touch $PWD/$nom_dossier_chiffre/$file
  #echo "./shift -r $file -s $cle_chiffrement > $PWD/$nom_dossier_chiffre/$file"
  ./shift -r $file -s $cle_chiffrement > $PWD/$nom_dossier_chiffre/$file
done

#calcule le hash de la cle 
hash=`echo -n $cle_chiffrement | md5sum -b | awk '{print $1}'`
echo "Le hash MD5 de l'argument $cle_chiffrement est : $hash"
echo $hash > $PWD/$nom_dossier_chiffre"/hash.txt"