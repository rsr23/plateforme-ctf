#!/usr/bin/env bash

/usr/sbin/sshd -D &
while true; do sleep 10; python3 /app/script_scapy.py; done #Attente semi passive pour éviter le flood de la sortie standard du container ainsi que la surconsomation CPU lors de l'appelle de "sudo rick-et-morty -r"
