-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: challenge
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- CREATE Database challenge;
-- USE challenge;

--
-- Table structure for table 'users'
--

DROP TABLE IF EXISTS 'users';
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE 'users' (
  'id' int AUTO_INCREMENT NOT NULL,
  'login' varchar(20) NOT NULL,
  'password' varchar(50) NOT NULL,
  'description' varchar(100) DEFAULT NULL,
  PRIMARY KEY ('id')
);

--
-- Dumping data for table 'users'
--

-- LOCK TABLES 'users' WRITE;
/*!40000 ALTER TABLE 'users' DISABLE KEYS */;
INSERT INTO 'users' VALUES (
  4,'admin','admin','Vous avez essayé de faire des expressions régulières ?'),
  (5, 'PasRick', 'StGermain', 'Je suis un utilisateur lambda qui aime le foot'),
  (6,'MonsieurGobtou','GFaim','Je gobe tout faites attention'),
  (7,'jerry','ç4v4squ4nch3r','YxaeFZqP4QBJRphFMHorX3qdGUhpmj'),
  (8, 'test', 'test', 'Ceci est un utilisateur de test');
/*!40000 ALTER TABLE 'users' ENABLE KEYS */;
-- UNLOCK TABLES;

DROP TABLE IF EXISTS 'uuussseeerrrsss';
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE 'uuussseeerrrsss' (
  'id' int AUTO_INCREMENT NOT NULL,
  'login' varchar(20) NOT NULL,
  'password' varchar(50) NOT NULL,
  'description' varchar(100) DEFAULT NULL,
  PRIMARY KEY ('id')
);

INSERT INTO 'uuussseeerrrsss' VALUES (
  4,'admin','21232f297a57a5a743894a0e4a801fc3','Une injection simple dans le formulaire de login pourrait fonctionner ...');

--
-- Dumping routines for database 'challenge'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-23 22:38:34
