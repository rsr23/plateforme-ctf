terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
    }
  }
}

provider "docker" {
  host = "ssh://${var.dockeruser}@${var.dockerhost}:${var.dockerport}"
}

# region rick-et-mortyweb

resource "docker_container" "docker_image_rick-et-morty-web" {
  # using rtmp interception kali image
  image = "${var.docker_registry}/rick-et-morty-web:${var.build_version}"
  name  = "docker_${var.instance_id}_rick-et-morty-web"
  ports {
    internal = 80
  }
}

# endregion

# region rick-et-mortycrypto

resource "docker_container" "docker_image_rick-et-morty-crypto" {
  image = "${var.docker_registry}/rick-et-morty-crypto:${var.build_version}"
  name  = "docker_${var.instance_id}_rick-et-morty-crypto"
  networks_advanced {
    name = docker_network.docker_private_network_main.name
  }
  ports {
    internal = 22
  }
}

# endregion

# region rick-et-morty-scapy-cromulon

# resource "docker_container" "docker_image_rick-et-morty-scapy-cromulon" {
#   image = "${var.docker_registry}/rick-et-morty-scapy-cromulon:${var.build_version}"
#   name  = "docker_${var.instance_id}_rick-et-morty-scapy-cromulon"
#   networks_advanced {
#     name = docker_network.docker_private_network_main.name
#   }
# }

# endregion

# region rick-et-mortyscapy-rick

# resource "docker_container" "docker_image_rick-et-morty-scapy-rick" {
#   image = "${var.docker_registry}/rick-et-morty-scapy-rick:${var.build_version}"
#   name  = "docker_${var.instance_id}_rick-et-morty-scapy-rick"
#   networks_advanced {
#     name = docker_network.docker_private_network_main.name
#   }
# }

# endregion


# region Networks

resource "docker_network" "docker_private_network_main" {
  name = "docker_${var.instance_id}_main_subnet"
}

# endregion

#region Variables

variable "instance_id" {
  type = number
}

variable "dockerhost" {
  type = string
}

variable "dockerport" {
  type = string
}

variable "dockeruser" {
  type = string
}

variable "docker_registry" {
  type = string
}

variable "build_version" {
  type = string
}

# output "port22_scapy-cromulon" {
#   value = docker_container.docker_image_rick-et-morty-scapy-cromulon.ports[0].external
# }
output "port22_crypto" {
  value = docker_container.docker_image_rick-et-morty-crypto.ports[0].external
}
output "port80_web" {
  value = docker_container.docker_image_rick-et-morty-web.ports[0].external
}

# endregion
